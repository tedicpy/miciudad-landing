---
title: "Contact"
date: 2018-01-31T12:44:01-03:00
---

Si tienes **dudas** o **sugerencias** sobre MiCiudad Encarnación, puedes escribirnos a la casilla de correo electrónico o a la cuenta de Twitter que te dejamos a continuación:
