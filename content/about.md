---
title: "About"
date: 2018-01-31T12:43:56-03:00
---

*MiCiudad Encarnación* es un proyecto que permitirá **facilitar la gestión de la ciudad**, a través de una herramienta Web que localice geográficamente propuestas, eventos y reportes por parte de usuarios y organizaciones, y que a su vez permitirá a la Municipalidad, **responder a las mismas de forma rápida y efectiva**.

Un primer prototipo fue apoyado por *Hivos/ASDI* mientras que el desarrollo actual está apoyado por el Banco *Interamericano de Desarrollo*.

En el proyecto participan ... (aquí van las organizaciones) 

Este es un proyecto de [SENATICS](https://www.senatics.gov.py/), [Municipalidad de Encarnación](http://encarnacion.gov.py/) y [TEDIC](https://www.tedic.org/). 
